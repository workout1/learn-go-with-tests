package shapes

import (
	"math"
)

// Shape is a shape interface
type Shape interface {
	Area() float64
}

// Rectangle is a ...
type Rectangle struct {
	Width  float64
	Height float64
}

// Circle is a ...
type Circle struct {
	Radius float64
}

// Triangle is a ...
type Triangle struct {
	Base   float64
	Height float64
}

// Perimeter calculates the perimeter of a rectangle
func (r Rectangle) Perimeter() float64 {
	return 2 * (r.Width + r.Height)
}

// Area calculates the area of a rectangle, method of Rectangle struct
func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

// Perimeter calculates the perimeter of a rectangle
func Perimeter(r Rectangle) float64 {
	return 2 * (r.Width + r.Height)
}

// Area calculates the area of a Rectangle
func Area(r Rectangle) float64 {
	return r.Width * r.Height
}

// Area calculates the area of a Circle
func (c Circle) Area() float64 {
	return math.Pow(c.Radius, 2) * math.Pi
}

// Area calculates the area of a Triangle
func (t Triangle) Area() float64 {
	return t.Base * t.Height * 0.5
}
