package wallet

import (
	"errors"
	"fmt"
)

// Token is a crypto monkey
type Token float64

// String to implement the Stringer interface
func (t Token) String() string {
	return fmt.Sprintf("%f TOK", t)
}

// Wallet mimicks a wallet on the crypto world
type Wallet struct {
	balance Token
}

var (
	ErrNotEnoughBalance = errors.New("Not enough Token in wallet")
)

// Deposit is
func (w *Wallet) Deposit(d Token) {
	w.balance += d
}

// Balance is
func (w *Wallet) Balance() Token {
	return w.balance
}

// Withdraw is to withdraw
func (w *Wallet) Withdraw(amount Token) error {
	if amount > w.balance {
		return ErrNotEnoughBalance
	}
	w.balance -= amount
	return nil
}
