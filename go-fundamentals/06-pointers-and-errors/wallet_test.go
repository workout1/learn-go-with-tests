package wallet

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWallet(t *testing.T) {
	t.Run("Deposit", func(t *testing.T) {
		wallet := &Wallet{}

		wallet.Deposit(Token(10))

		got := wallet.Balance()
		want := Token(10.0)

		if got != want {
			t.Errorf("got %s want %s", got, want)
		}
	})

	t.Run("Withdraw", func(t *testing.T) {
		t.Run("[NO ERROR] Enough balance", func(t *testing.T) {
			wallet := &Wallet{balance: Token(20)}

			wallet.Withdraw(Token(10))

			got := wallet.Balance()
			want := Token(10.0)

			if got != want {
				t.Errorf("got %s want %s", got, want)
			}
		})
		t.Run("[ERROR] Not enough balance", func(t *testing.T) {
			wallet := &Wallet{balance: Token(20)}

			err := wallet.Withdraw(Token(100))

			assert.Error(t, err, ErrNotEnoughBalance)
		})
	})
}
