package dictionary

// Dictionary is
type Dictionary map[string]string

const (
	ErrNotFoundInDictionary = DictionaryErr("could not find the word you were looking for")
	ErrWordExists           = DictionaryErr("word already exists in dictionary")
	ErrWordDoesNotExist     = DictionaryErr("word does not exist in the dictionary")
)

type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

// Search is the Dictionary receiver search function
func (d Dictionary) Search(text string) (string, error) {
	value, ok := d[text]
	if !ok {
		return "", ErrNotFoundInDictionary
	}
	return value, nil
}

func (d Dictionary) Add(key, text string) error {
	_, err := d.Search(key)

	switch err {
	case ErrNotFoundInDictionary:
		d[key] = text
	case nil:
		return ErrWordExists
	default:
		return err
	}

	return nil
}

func (d Dictionary) Update(key, text string) error {
	_, err := d.Search(key)
	switch err {
	case ErrNotFoundInDictionary:
		return ErrWordDoesNotExist
	case nil:
		d[key] = text
	default:
		return err

	}
	return nil
}

func (d Dictionary) Delete(word string) {
	delete(d, word)
}

// Search searches a lot
func Search(dict map[string]string, text string) string {
	return dict[text]
}
