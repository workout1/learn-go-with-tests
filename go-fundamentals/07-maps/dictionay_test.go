package dictionary

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSearch(t *testing.T) {
	dictionary := Dictionary{"test": "this is just a test"}
	t.Run("[NO ERROR] search found", func(t *testing.T) {
		got, _ := dictionary.Search("test")
		want := "this is just a test"

		assert.Equal(t, want, got)
	})
	t.Run("[ERROR] search NOT found", func(t *testing.T) {
		_, err := dictionary.Search("unkown")

		assert.Error(t, err, ErrNotFoundInDictionary)
	})
}

func TestAdd(t *testing.T) {
	dictionary := Dictionary{}
	dictionary.Add("test", "this is just a test")

	want := "this is just a test"
	got, err := dictionary.Search("test")
	if err != nil {
		t.Fatal("should find added word:", err)
	}

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}

	t.Run("new word", func(t *testing.T) {
		dictionary := Dictionary{}
		word := "test"
		definition := "this is just a test"

		err := dictionary.Add(word, definition)
		assert.Nil(t, err)

		got, err := dictionary.Search(word)
		assert.Nil(t, err)

		assert.Equal(t, definition, got)
	})

	t.Run("existing word", func(t *testing.T) {
		word := "test"
		definition := "this is just a test"
		dictionary := Dictionary{word: definition}
		err := dictionary.Add(word, "new test")

		assert.Error(t, err, ErrWordExists)

		got, err := dictionary.Search(word)
		assert.Nil(t, err)
		assert.Equal(t, definition, got)
	})
}

func TestUpdate(t *testing.T) {
	word := "test"
	definition := "this is just a test"
	dictionary := Dictionary{word: definition}
	newDefinition := "new definition"

	dictionary.Update(word, newDefinition)

	text, err := dictionary.Search(word)

	assert.Nil(t, err)
	assert.Equal(t, newDefinition, text)
}

func TestDelete(t *testing.T) {
	word := "test"
	dictionary := Dictionary{word: "test definition"}

	dictionary.Delete(word)

	_, err := dictionary.Search(word)
	if err != ErrNotFoundInDictionary {
		t.Errorf("Expected %q to be deleted", word)
	}
}
