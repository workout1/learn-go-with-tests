package greet

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGreet(t *testing.T) {
	buffer := bytes.Buffer{}
	Greet(&buffer, "Chris")

	got := buffer.String()
	want := "Hello, Chris"

	assert.Equal(t, want, got)
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
