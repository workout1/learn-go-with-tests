package concurrency

// WebsiteChecker is a checker
type WebsiteChecker func(string) bool

type result struct {
	string
	bool
}

// CheckWebsites checks web sites
func CheckWebsites(wc WebsiteChecker, sites []string) map[string]bool {
	results := make(map[string]bool, len(sites))

	resultChannel := make(chan result)
	for _, url := range sites {
		go func(u string) {
			resultChannel <- result{u, wc(u)}

		}(url)
	}

	for i := 0; i < len(sites); i++ {
		r := <-resultChannel
		results[r.string] = r.bool
	}

	return results
}
