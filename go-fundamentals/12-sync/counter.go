package counter

import "sync"

// Counter counts
type Counter struct {
	value int
	mutex sync.Mutex
}

// NewCounter returns a pointer to a Counter
func NewCounter() *Counter {
	return &Counter{}
}

// Inc increments
func (c *Counter) Inc() {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.value++
}

// Value gets the value
func (c *Counter) Value() int {
	return c.value
}
